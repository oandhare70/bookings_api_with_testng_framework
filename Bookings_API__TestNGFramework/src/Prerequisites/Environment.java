package Prerequisites;

public class Environment {
	
	public static String Hostname() {
		String hostname = "https://restful-booker.herokuapp.com/";
		return hostname;
	}
	
	public static String HeaderName() {

		String headername = "Content-Type";
		return headername;
	}

	public static String HeaderValue() {

		String headervalue = "application/json";
		return headervalue;
	}
	
	
	public static String[] Headername_forUpdate() {
		String[] headername = {"Content-Type","Accept"};
		return headername;
	}
	public static String[] Headervalue_forUpdate() {
		String[] headervalue = {"application/json","application/json"};
		return headervalue;
	}
	
	
	public static String resource_APICheck() {
		String resource = "ping";
		return resource;
	}
	
	public static String resource_createBooking() {
		String resource = "booking";
		return resource;
	}
	
	public static String resource_getBookings() {
		String resource = "booking/";
		return resource;
	}
	public static String resource_updateBooking() {
		String resource = "booking/";
		return resource;
	}
	public static String resource_deleteBooking() {
		String resource = "booking/";
		return resource;
	}
	public static String resource_Auth() {
		String resource = "auth";
		return resource;
	}
//	public static String[] headernameforauth() {
//		String[] headername= {"Content-Type","Cookies"};
//		return headername;
//	}
//	
//	public static String[] headervalueforauth() {
//		String[] headervalue= {"application/json","token="};
//		return headervalue;
//	}

}
