package testcases_withDifferentApproaches;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import org.testng.annotations.Parameters; 

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


public class CreateBooking_DataDriven_testNGxml extends RequestBody {

	public static String bookingId;

	@Parameters({"firstname","lastname","totalprice","depositpaid","checkin","checkout","additionalneeds"})
	@Test
	
	public static void Executor(String firstname,String lastname,String totalprice,String depositpaid,String checkin,String checkout,String additionalneeds) throws IOException {

		File Folder = Utility.createLogDirectory("CreateBooking_Log");
		String reqBody =  "{\r\n"
				+ "    \"firstname\" : \""+firstname+"\",\r\n"
				+ "    \"lastname\" : \""+lastname+"\",\r\n"
				+ "    \"totalprice\" : "+totalprice+",\r\n"
				+ "    \"depositpaid\" : "+depositpaid+",\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \""+checkin+"\",\r\n"
				+ "        \"checkout\" : \""+checkout+"\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \""+additionalneeds+"\"\r\n"
				+ "}";;
		String Endpoint = RequestBody.Hostname() + RequestBody.resource_createBooking();
		int Statuscode = 0;

		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.createBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(),
					reqBody, Endpoint);

			Statuscode = response.statusCode();
			ResponseBody resbody = response.getBody();
			bookingId = resbody.jsonPath().getString("bookingid");

			
			if (Statuscode == 200) {
				Utility.CreateEvidenceFile(Folder, Utility.createFileName("createBookingLog"), Endpoint, reqBody,
						response.getHeader("date"), response.getBody().asPrettyString(), bookingId);
				validator(response, reqBody);
				break;
			} else {
				System.out.println("Expected statuscode is not found at" + i + "hence retrying");
			}

		}
		if (Statuscode != 200) {
			System.out.println("expected status code is not found after5 iteration hence booking is not created");
		}

	}

	public static void validator(Response response, String reqbody) {
		ResponseBody resbody = response.getBody();

		String res_firstname = resbody.jsonPath().getString("booking.firstname");
		String res_lastname = resbody.jsonPath().getString("booking.lastname");
		String res_totalprice = resbody.jsonPath().getString("booking.totalprice");

		String res_depositpaid = resbody.jsonPath().getString("booking.depositpaid");
		String res_checkindate = resbody.jsonPath().getString("booking.bookingdates.checkin");
		String res_checkoutdate = resbody.jsonPath().getString("booking.bookingdates.checkout");

		String res_additionalneeds = resbody.jsonPath().getString("booking.additionalneeds");

		// fetch request body parameters by using Jsonpath()
		JsonPath jpathreqbody = new JsonPath(reqbody);
		String req_firstname = jpathreqbody.getString("firstname");

		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");

		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");
		// validate response with testngs assert

		Assert.assertNotNull(bookingId);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		Assert.assertEquals(res_depositpaid, req_depositpaid);
		Assert.assertNotNull(res_checkindate);
		Assert.assertEquals(res_additionalneeds, req_additionalneeds);

	}

}
