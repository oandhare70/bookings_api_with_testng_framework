package TestCases_DataDrivenWithDataProvider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _3_CreateBooking extends RequestBody{

    static String bookingId;
	File Folder;
	String reqBody;
	String Endpoint;
	int Statuscode = 0;
	Response response;
	
	
	
	@BeforeTest
	public void testsetup() throws IOException {

		
		Folder = Utility.createLogDirectory("CreateBooking_LogDataProvider");
		Endpoint = RequestBody.Hostname() + RequestBody.resource_createBooking();
		
	}
//	dataProviderClass = Prerequisites.RequestBody.class,
	@Test(retryAnalyzer=RetryAnalyzer.class,dataProvider = "CreateBooking_2", dataProviderClass=Prerequisites.RequestBody.class, description = "Data provider Annotation in different class")
	public void validator(String firstname, String lastname, String totalprice, String depositpaid, String checkin,
	                      String checkout, String additionalneeds) throws IOException {
		System.out.println(" create booking test method called");
		reqBody = "{\r\n" + "    \"firstname\" : \"" + firstname + "\",\r\n" + "    \"lastname\" : \"" + lastname
				+ "\",\r\n" + "    \"totalprice\" : " + totalprice + ",\r\n" + "    \"depositpaid\" : " + depositpaid
				+ ",\r\n" + "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"" + checkin + "\",\r\n"
				+ "        \"checkout\" : \"" + checkout + "\"\r\n" + "    },\r\n" + "    \"additionalneeds\" : \""
				+ additionalneeds + "\"\r\n" + "}";
		response = API_Trigger.createBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), reqBody, Endpoint);
		ResponseBody resbody = response.getBody();
		bookingId = resbody.jsonPath().getString("bookingid");
		System.out.println(bookingId);
//		response = API_Trigger.createBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), reqBody, Endpoint);

//		ResponseBody resbody = response.getBody();
		int Statuscode = response.statusCode();
		bookingId = resbody.jsonPath().getString("bookingid");

		String res_firstname = resbody.jsonPath().getString("booking.firstname");
		String res_lastname = resbody.jsonPath().getString("booking.lastname");
		String res_totalprice = resbody.jsonPath().getString("booking.totalprice");

		String res_depositpaid = resbody.jsonPath().getString("booking.depositpaid");
		String res_checkindate = resbody.jsonPath().getString("booking.bookingdates.checkin");
		String res_checkoutdate = resbody.jsonPath().getString("booking.bookingdates.checkout");

		String res_additionalneeds = resbody.jsonPath().getString("booking.additionalneeds");

		// fetch request body parameters by using Jsonpath()
		JsonPath jpathreqbody = new JsonPath(reqBody);
		String req_firstname = jpathreqbody.getString("firstname");

		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");

		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");
		// validate response with testngs assert

		Assert.assertNotNull(bookingId);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		Assert.assertEquals(res_depositpaid, req_depositpaid);
		Assert.assertNotNull(res_checkindate);
		Assert.assertEquals(res_additionalneeds, req_additionalneeds);
		
		Utility.CreateEvidenceFile(Folder, Utility.createFileNameforDataDriven("createBookingLog_dataproviderAnnotation"), Endpoint, reqBody,response.getHeader("date"), response.getBody().asPrettyString(), bookingId);

	}

	@AfterTest
	public void CreateEvidenceFile() throws IOException {
		System.out.println("Test Execution is done");
		
	}
	

}
