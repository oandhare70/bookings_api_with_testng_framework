package TestCases_DataDrivenWithDataProvider;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _6_getUpdatedBooking {

	File folder;
	String endpoint;
	int Statuscode;
	String bookingId;
	Response response;

	@BeforeTest
	public void testsetup() {
		System.out.println("before test method called");
		folder = Utility.createLogDirectory("GetUpdatedBooking_LogDataProvider");
		
	}
	@Test(retryAnalyzer=RetryAnalyzer.class,dependsOnMethods = "TestCases_DataDrivenWithDataProvider._5_UpdateBooking.validator" ,description="validate response body parameters of getting updated booking")
	public void Executor() throws IOException {
		System.out.println("test method called");
		bookingId = _3_CreateBooking.bookingId;
		endpoint = RequestBody.Hostname() + RequestBody.resource_getBookings() + bookingId;
		System.out.println(endpoint);
		response = API_Trigger.getBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), endpoint);
		Statuscode = response.statusCode();
		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("GETUpdatedBooking"), endpoint,
				"Request body is not given", response.getHeader("date"), response.getBody().asPrettyString(),
				bookingId);

	}

	@AfterTest
	public void createevidencefile() throws IOException {
		System.out.println("getUpdatedBooking class after test method called");
//		Utility.CreateEvidenceFile(folder, Utility.createFileName("GETUpdatedBooking"), endpoint,
//				"Request body is not given", response.getHeader("date"), response.getBody().asPrettyString(),
//				bookingId);
	}

}
