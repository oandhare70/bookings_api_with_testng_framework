package TestCases_DataDrivenWithDataProvider;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.response.Response;

public class _7_DeleteBooking {

	File folder;
	String endpoint;
	int Statuscode;
	Response response;
	String bookingId;

	@BeforeTest
	public void testsetup() {
		System.out.println("before test method called");
		folder = Utility.createLogDirectory("DeleteBooking_LogDataProvider");
	}

	@Test(retryAnalyzer=RetryAnalyzer.class,dependsOnMethods = "TestCases_DataDrivenWithDataProvider._2_CreateAuth.validator", description = "validate response body parameter of deleting booking data")
	public void Executor() throws IOException {
		System.out.println("test method called");
		String bookingId = _3_CreateBooking.bookingId;
		System.out.println(bookingId);
		endpoint = RequestBody.Hostname() + RequestBody.resource_deleteBooking() + bookingId;
		response = API_Trigger.DeleteBooking(new String[] { "Content-Type", "Cookie" },
				new String[] { "application/json", "token=" + _2_CreateAuth.token }, "no need to have booking id",
				endpoint);

		Statuscode = response.getStatusCode();
		System.out.println(Statuscode);
		if (response.getStatusCode() == 201) {
			System.out.println("Booking with ID " + bookingId + " is deleted");
		} else {
			System.out.println(
					"Booking is not being able to delete because the booking id is null or authentication error must be there");
		}
		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("DeleteBooking"), endpoint,
				"no need to have booking id", response.getHeader("date"), response.getBody().asPrettyString(), null);

	}

	@AfterTest
	public void createEvidenceFile() throws IOException {
		System.out.println("after test method called");
	}

}
