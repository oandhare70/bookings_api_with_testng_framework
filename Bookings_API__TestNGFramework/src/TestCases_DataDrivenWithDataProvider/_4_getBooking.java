package TestCases_DataDrivenWithDataProvider;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.response.Response;

public class _4_getBooking {

    File folder;
    String endpoint;
    Response response;
    String bookingId;

    @BeforeTest
    public void testSetup() {
        System.out.println("Before test method called");
        folder = Utility.createLogDirectory("GetBooking_LogDataProvider");
        
    }

    @Test(retryAnalyzer=RetryAnalyzer.class,dependsOnMethods = "TestCases_DataDrivenWithDataProvider._3_CreateBooking.validator" ,description="validate response body parameters of get booking data")
    public void Executor() throws IOException {
        System.out.println("Test method called");
        bookingId = _3_CreateBooking.bookingId; // Fetching the booking ID
        endpoint = RequestBody.Hostname() + RequestBody.resource_getBookings() + bookingId;
        
        response = API_Trigger.getBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), endpoint);
//        System.out.println("Response status code: " + response.getStatusCode());
//        System.out.println("Response body: " + response.getBody().asPrettyString());
        Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("GETBooking"), endpoint,
                "Request body is not given", response.getHeader("date"), response.getBody().asPrettyString(),
                bookingId);
    }

    @AfterTest
    public void createEvidenceFile() throws IOException {
        System.out.println(" getBooking data After test method called");
//        System.out.println(endpoint);
       
    }
}
