package TestCases_DataDrivenWithDataProvider;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _5_UpdateBooking {

	File folder;
	String Endpoint;
	String reqbody;
	Response response;
	int statuscode;
	String bookingId;

	@BeforeTest
	public void testsetup() throws IOException {
		System.out.println("before test method called");
		folder = Utility.createLogDirectory("UpdateBooking_LogDataProvider");
//		bookingId = _3_CreateBooking.bookingId;
//		System.out.println(bookingId);
	}

	@Test(retryAnalyzer=RetryAnalyzer.class, dataProvider = "updateBooking_2", dataProviderClass = Prerequisites.RequestBody.class, description = "validate the response body parameters of booking data updation",dependsOnMethods="TestCases_DataDrivenWithDataProvider._3_CreateBooking.validator")
	public void validator(String firstname, String lastname, String totalprice, String depositpaid, String checkin,
			String checkout, String additionalneeds) throws IOException {
		System.out.println("_5_UpdateBooking test method called");

		reqbody = "{\r\n" + "    \"firstname\" : \"" + firstname + "\",\r\n" + "    \"lastname\" : \"" + lastname
				+ "\",\r\n" + "    \"totalprice\" : " + totalprice + ",\r\n" + "    \"depositpaid\" : " + depositpaid
				+ ",\r\n" + "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"" + checkin + "\",\r\n"
				+ "        \"checkout\" : \"" + checkout + "\"\r\n" + "    },\r\n" + "    \"additionalneeds\" : \""
				+ additionalneeds + "\"\r\n" + "}";

		bookingId = _3_CreateBooking.bookingId;
		System.out.println(bookingId);
		Endpoint = RequestBody.Hostname() + RequestBody.resource_updateBooking() + bookingId;
System.out.println(Endpoint);
System.out.println(_2_CreateAuth.token);
		response = API_Trigger.UpdateBooking(new String[] { "Content-Type", "Cookie" },
				new String[] { "application/json", "token=" + _2_CreateAuth.token }, reqbody, Endpoint);
System.out.println(":::"+response.asPrettyString());
		statuscode = response.statusCode();

		ResponseBody resbody = response.getBody();

		String res_firstname = resbody.jsonPath().getString("firstname");
		String res_lastname = resbody.jsonPath().getString("lastname");
		String res_totalprice = resbody.jsonPath().getString("totalprice");
		String res_depositpaid = resbody.jsonPath().getString("depositpaid");
		String res_checkindate = resbody.jsonPath().getString("bookingdates.checkin");
		String res_checkoutdate = resbody.jsonPath().getString("bookingdates.checkout");

		String res_additionalneeds = resbody.jsonPath().getString("additionalneeds");

		// fetch request body parameters by using Jsonpath()
		JsonPath jpathreqbody = new JsonPath(reqbody);
		String req_firstname = jpathreqbody.getString("firstname");
//		System.out.println(req_firstname);
		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");
		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates.checkin");
		String req_checkoutdate = jpathreqbody.getString("bookingdates.checkout");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");
		// validate response with testngs assert

//		Assert.assertNotNull(CreateBooking.bookingId);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		Assert.assertEquals(res_depositpaid, req_depositpaid);
//		Assert.assertEquals(res_checkindate, req_checkindate);
//		Assert.assertEquals(res_checkoutdate, req_checkoutdate);
		Assert.assertEquals(res_additionalneeds, req_additionalneeds);

		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("UpdateBooking"), Endpoint, reqbody,
				response.getHeader("date"), response.getBody().asPrettyString(), bookingId);

	}

	@AfterTest
	public void createEvidenceFile() throws IOException {
		System.out.println("UpdateBooking after test method called");

	}
}