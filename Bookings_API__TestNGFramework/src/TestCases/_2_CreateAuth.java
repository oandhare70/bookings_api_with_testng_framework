package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class _2_CreateAuth {

	public static String token;
	File folder;
	String Endpoint;
	int Statuscode = 0;
	Response response;

	@BeforeTest
	public void testsetup() {
		System.out.println("Before test method called");
		folder = Utility.createLogDirectory("CreateAuth_Log");
		Endpoint = RequestBody.Hostname() + RequestBody.resource_Auth();
		response = API_Trigger.Auth(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				RequestBody.AuthBody(), Endpoint);

	}

	@Test(retryAnalyzer=RetryAnalyzer.class,description="validate response body parameters of create Authenctication token")
	public void Executor() throws IOException {
		System.out.println("test method called");
		response = API_Trigger.Auth(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				RequestBody.AuthBody(), Endpoint);
//		System.out.println(response.getBody().asPrettyString());
		ResponseBody responseBody = response.getBody();
		token = responseBody.jsonPath().getString("token");
		Assert.assertEquals(response.statusCode(), 200);

	}

	@AfterTest
	public void CreateEvidenceFile() throws IOException {
		System.out.println("After test method called");
		Utility.CreateEvidenceFile(folder, Utility.createFileName("CreateAuth"), Endpoint, null,
				response.getHeader("date"), response.getBody().asPrettyString(), null);

	}
}
