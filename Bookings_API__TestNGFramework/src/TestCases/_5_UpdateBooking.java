package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _5_UpdateBooking {

	File folder;
	String Endpoint;
	String reqbody;
	Response response;
	int statuscode;
	String bookingId;

	@BeforeTest
	public void testsetup() throws IOException {
		System.out.println("before test method called");
		folder = Utility.createLogDirectory("UpdateBooking_Log");
		reqbody = RequestBody.updateBooking("TestCase_2");

	}

	@Test(retryAnalyzer=RetryAnalyzer.class,description="validate the response body parameters of booking data updation")
	public void Executor() throws IOException {
		System.out.println("_5_UpdateBooking test method called");

		bookingId =_3_CreateBooking.bookingId;
		Endpoint = RequestBody.Hostname() + RequestBody.resource_updateBooking() + bookingId;
		response = API_Trigger.UpdateBooking(new String[] { "Content-Type", "Cookie" },
				new String[] { "application/json", "token=" + _2_CreateAuth.token }, reqbody, Endpoint);

		statuscode = response.statusCode();

		ResponseBody resbody = response.getBody();

		String res_firstname = resbody.jsonPath().getString("firstname");
		String res_lastname = resbody.jsonPath().getString("lastname");
		String res_totalprice = resbody.jsonPath().getString("totalprice");
		String res_depositpaid = resbody.jsonPath().getString("depositpaid");
		String res_checkindate = resbody.jsonPath().getString("bookingdates.checkin");
		String res_checkoutdate = resbody.jsonPath().getString("bookingdates.checkout");

		String res_additionalneeds = resbody.jsonPath().getString("additionalneeds");

		// fetch request body parameters by using Jsonpath()
		JsonPath jpathreqbody = new JsonPath(reqbody);
		String req_firstname = jpathreqbody.getString("firstname");
//		System.out.println(req_firstname);
		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");
		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates.checkin");
		String req_checkoutdate = jpathreqbody.getString("bookingdates.checkout");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");
		// validate response with testngs assert

//		Assert.assertNotNull(CreateBooking.bookingId);
		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		Assert.assertEquals(res_depositpaid, req_depositpaid);
//		Assert.assertEquals(res_checkindate, req_checkindate);
//		Assert.assertEquals(res_checkoutdate, req_checkoutdate);
		Assert.assertEquals(res_additionalneeds, req_additionalneeds);

	}

	@AfterTest

	public void createEvidenceFile() throws IOException {
		System.out.println("_5_UpdateBooking after test method called");
		Utility.CreateEvidenceFile(folder, Utility.createFileName("UpdateBooking"), Endpoint, reqbody,
				response.getHeader("date"), response.getBody().asPrettyString(), null);

	}
}