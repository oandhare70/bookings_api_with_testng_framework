package TestCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.RetryAnalyzer;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _1_Api_HealthCheck {

	File folder;
	String Endpoint;
	int statuscode;
	Response response;
	@BeforeTest
	public void testSetUp() {
		System.out.println("Before test method called");
		folder = Utility.createLogDirectory("checkAPI_Log");
		Endpoint = RequestBody.Hostname() + RequestBody.resource_APICheck();
	}

	@Test(retryAnalyzer=RetryAnalyzer.class,description="validate the response body parameters of api_healthCheck")
	public void Executor() throws IOException {
		
		System.out.println("test method called");
		response = API_Trigger.ApiCheck(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);
		ResponseBody resbody = response.getBody();
		statuscode = response.getStatusCode();
		Assert.assertEquals(statuscode, 201);
System.out.println("executor ends");
	}

	@AfterTest
	public void CreateEvidenceFile() throws IOException {
		System.out.println("After test method called");
		Utility.CreateEvidenceFile(folder, Utility.createFileName("CheckAPILog"), Endpoint, "request body is not given",
				response.getHeader("date"), response.getBody().asPrettyString(), null);
	}

}
