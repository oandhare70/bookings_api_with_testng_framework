endpoint is : https://restful-booker.herokuapp.com/booking



requestbody is : {
    "firstname" : "Pratap",
    "lastname" : "Kale",
    "totalprice" : 333,
    "depositpaid" : false,
    "bookingdates" : {
        "checkin" : "2024-03-03",
        "checkout" : "2024-03-17"
    },
    "additionalneeds" : "Dinner"
}



Trigger time is : Thu, 14 Mar 2024 17:47:10 GMT



created booking id is: 3471

response body is : {
    "bookingid": 3471,
    "booking": {
        "firstname": "Pratap",
        "lastname": "Kale",
        "totalprice": 333,
        "depositpaid": false,
        "bookingdates": {
            "checkin": "2024-03-03",
            "checkout": "2024-03-17"
        },
        "additionalneeds": "Dinner"
    }
}

